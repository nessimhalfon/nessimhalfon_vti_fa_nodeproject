const Books = require("../models/books.model");

const controller = {


    getAll: (req, res, next) => {
        Books.find({})
            .then(
                books => {
                    res.statusCode = 200;
                    res.setHeader("Content-Type", "application/json");
                    res.json(books);
                },
                err => next(err)
            )
            .catch(err => next(err));
    },

    addOne: (req, res, next) => {
        var token = getToken(req.headers);
        if (token) {
            Books.create(req.body)
                .then(
                    book => {
                        res.statusCode = 200;
                        res.setHeader("Content-Type", "application/json");
                        res.json(book);
                    },
                    err => next(err)
                )
                .catch(err => next(err));
        } else {
            return res.status(403).send({ success: false, msg: "Not authenticate" });
        }
    },

    getOne: (req, res, next) => {
        Books.findById(req.params.bookId)
            .then(
                book => {
                    res.statusCode = 200;
                    res.setHeader("Content-Type", "application/json");
                    res.json(book);
                },
                err => next(err)
            )
            .catch(err => next(err));
    },

    updateOne: (req, res, next) => {
        var token = getToken(req.headers);
        if (token) {
            Books.findByIdAndUpdate(req.params.bookId, { $set: req.body }, { new: true })
                .then(
                    book => {
                        res.statusCode = 200;
                        res.setHeader("Content-Type", "application/json");
                        res.json(book);
                    },
                    err => next(err)
                )
                .catch(err => next(err));
        } else {
            return res.status(403).send({ success: false, msg: "Not authenticate" });
        }

    },

    deleteOne: (req, res, next) => {
        var token = getToken(req.headers);
        if (token) {
            Books.findByIdAndRemove(req.params.bookId)
                .then(
                    resp => {
                        res.statusCode = 200;
                        res.setHeader("Content-Type", "application/json");
                        res.json(resp);
                    },
                    err => next(err))
                .catch(err => next(err));
        } else {
            return res.status(403).send({ success: false, msg: "Not authenticate" });
        }
    }
}

getToken = function (headers) {
    if (headers && headers.authorization) {
        var parted = headers.authorization.split(' ');
        if (parted.length === 2) {
            return parted[1];
        } else {
            return null;
        }
    } else {
        return null;
    }
};

module.exports = controller;


