const Books = require("../models/books.model");

const controller_comments = {

    getAll: (req, res, next) => {
        Books.findById(req.params.bookId)
            .then(
                books => {
                    res.statusCode = 200;
                    res.setHeader("Content-Type", "application/json");
                    res.json(books.comments);
                },
                err => next(err)
            )
            .catch(err => next(err));
    },

    addOne: (req, res, next) => {
        var token = getToken(req.headers);
        if (token) {
            Books.findByIdAndUpdate(req.params.bookId, { $push: { comments: req.body }})
            .then(
                book => {
                    res.statutCode = 200;
                    res.setHeader("Content-Type", "application/json");
                    res.json(book.comments);
                },
                err => next(err)
            )
            .catch(err => next(err));
        }
        else {
            return res.status(403).send({success: false, msg: 'Not authenticate'});
        }
        
    },


    getOne: (req, res, next) => {
        Books.findById(req.params.bookId)
            .then(
                book => {
                    res.statusCode = 200;
                    res.setHeader("Content-Type", "application/json");
                    res.json(book.comments.id(req.params.commentId));
                },
                err => next(err)
            )
            .catch(err => next(err));
    },

    updateOne: (req, res, next) => {
        var token = getToken(req.headers);
        if (token) {
            Books.update({
                "_id": req.params.bookId,
                "comments._id": req.params.commentId
            }, {
                    $set: {
                        "comments.$.rating": req.body.rating,
                        "comments.$.comment": req.body.comment,
                        "comments.$.author": req.body.author
                    }
                },
                { new: true }
            )
                .then(
                    book => {
                        res.statutCode = 200;
                        res.setHeader("Content-Type", "application/json");
                        res.json(book);
                    },
                    err => next(err)
                )
                .catch(err => next(err));
        }
        else {
            return res.status(403).send({success: false, msg: 'Not authenticate'});
        }
        
    },

    deleteOne: (req, res, next) => {
        var token = getToken(req.headers);
        if (token) {
            Books.findByIdAndUpdate(req.params.bookId, { $pull: { comments: { "_id": req.params.commentId } } })
            .then(
                resp => {
                    res.statusCode = 200;
                    res.setHeader("Content-Type", "application/json");
                    res.json(resp);
                },
                err => next(err))
            .catch(err => next(err));
        }
        else {
            return res.status(403).send({success: false, msg: 'Not authenticate'});
        }
    }
}

getToken = function (headers) {
    if (headers && headers.authorization) {
        var parted = headers.authorization.split(' ');
        if (parted.length === 2) {
            return parted[1];
        } else {
            return null;
        }
    } else {
        return null;
    }
};

module.exports = controller_comments;