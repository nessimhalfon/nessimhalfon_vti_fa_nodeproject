const controller = require('../controllers/books.controller');
const controller_comments = require('../controllers/comments.controller');
const express = require('express');

const router = express.Router();
var passport = require("passport");

router.use(express.json());

router
    .route('/')
    .get(controller.getAll)
    .post(passport.authenticate('jwt', {session: false}), controller.addOne);

router
    .route('/:bookId')
    .get(controller.getOne)
    .put(passport.authenticate('jwt', {session: false}), controller.updateOne)
    .delete(passport.authenticate('jwt', {session: false}), controller.deleteOne);

router
    .route('/:bookId/comments')
    .get(controller_comments.getAll) 
    .post(controller_comments.addOne); 

router
    .route('/:bookId/comments/:commentId')
    .get(controller_comments.getOne) 
    .put(controller_comments.updateOne)
    .delete(controller_comments.deleteOne); 



module.exports = router;