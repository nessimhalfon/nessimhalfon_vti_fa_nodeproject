const mongoose = require("mongoose");
const Schema = mongoose.Schema;
require("mongoose-currency").loadType(mongoose);
const Currency = mongoose.Types.Currency;

const commentSchema = new Schema({
    rating  : {type: Number, min: 1, max: 5, required: true},
    comment : {type: String, required: true},
    author  : {type: mongoose.Schema.Types.ObjectId, ref: 'User'}
}, {timestamps: true});

const bookSchema = new Schema(
    {
        title           : { type: String, required: true },
        author          : { type: String, required: true },
        image           : { type: String },
        year            : { type: Number },
        price           : { type: Currency },
        bestSeller      : { type: Boolean, default: false },
        comments  : [commentSchema]
    },
    { timestamps: true }
);

const Books = mongoose.model("Books", bookSchema);

module.exports = Books;