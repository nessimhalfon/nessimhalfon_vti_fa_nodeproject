# Tutoriel Node-Express

![Node](https://upload.wikimedia.org/wikipedia/commons/thumb/d/d9/Node.js_logo.svg/1280px-Node.js_logo.svg.png)

Dans le cadre de l'UE Veille Technologie, nous allons devoir développer une application de vente de livre en ligne à l'aide de node.js et express.

## Introduction 

Dans de ce tutoriel nous allons apprendre à installer et utiliser Node avec express.

Node.js est une plateforme logicielle libre et événementielle en JavaScript orientée vers les applications réseau qui doivent pouvoir monter en charge.

## Installation de Node 

Dans un premier temps nous allons télécharger Node, rendez-vous sur le lien suivant : [Node](https://nodejs.org/en/download/).

## Installation de MongoDB

![MongoDB](https://blog.ippon.fr/content/images/2018/12/mongodb-logo-rgb.jpg)

Nous allons utiliser mongoDB comme base de données pour notre projet.

### Téléchargement de MongoDB

Pour télécharger mongoDB, rendez-vous sur le lien suivant : [mongoDB](https://docs.mongodb.com/manual/tutorial/install-mongodb-on-os-x/).

Déplacer le fichier télécharger vers un dossier de votre choix.

Rendez-vous à la racine de votre machine est en tapant la commande suivante : 

```
$ cd 
```

Nous allons nous rendre dans notre environnement de variable PATH : 

```
$ nano .bash_profile 
```

Ajoutez la ligne suivante : 

```
export PATH=<chemin ou se trouve le fichier télécharger précédemment>/bin:$PATH
```

Nous allons ensuite créer un dossier avec la commande suivante : 

```
$ sudo mkdir -p /data/db
```

### Création de la base de données amazon 

Nous allons créer une base de données nommé amazon grâce aux commandes suivantes : 

```
$ mongo 
> use amazon
> db.books.insert({title:"Game of Throne", author:" George R. R. Martin"})
```

### Lancement de mongoDB

Utilisez la commande suivante pour lancer mongoDB : 

```
$ mongod
```

## Installation du Framework Express 

![Express](https://expressjs.com/images/express-facebook-share.png)

Nous allons créer un dossier pour contenir notre application node et nous allons nous déplacer dans ce dossier.
Rendez-vous dans un terminal est entré les commandes suivantes : 

```
$ mkdir node-express
$ cd node-express
```

Nous allons installer express-generator qui va nous permettre de générer automatiquement une structure pour notre application node.

```
$ npm install express-generator -g
```

Dans le fichier `package.json`, rajoutez la ligne suivante : 

```
"scripts": {
    "start": "node ./bin/www"
  },
```

## Installation de Mongoose 

Nous allons installer mongoose, tapez la commande suivante : 

```
$ npm install mongoose --save 
```

Dans le fichier `app.js`, entrez les lignes de code suivantes :

```
var mongoose = require('mongoose');
var config = require('mongodb://localhost:27017/amazon')

const connect = mongoose.connect(config, {useNewUrlParser: true });
connect.then (
  db => {
    console.log("Connected correctly to the server");
  },
  err => {
    console.log(err);
  }
);
```

## Les Routes

Nous allons maintenant définir les différentes routes pour notre application.

Dans le fichier `app.js` nous allons ajouter les lignes suivantes.

```
...
var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users.routes');
var booksRouter = require('./routes/books.routes');
```

Il faut définir ces différentes routes. 

### Books

Rendez-vous dans le dossier `routes` et créez un fichier nommé `books.routes.js` dans lequel nous rentrerons les lignes suivantes :

```
const controller = require('../controllers/books.controller');
const express = require('express');

const router = express.Router();

router.use(express.json());
router
    .route('/')
    .get(controller.getAll)
    .post(controller.addOne);
    
router
    .route('/:bookId')
    .get(controller.getOne)
    .put(controller.updateOne)
    .delete(controller.deleteOne);
    
module.exports = router;
```

Nous allons maintenant ajouter les routes pour les commentaires d'un livre. Pour cela toujours dans le fichier `books.routes.js` ajoutez les lignes de code suivantes : 

```
const controller_comments = require('../controllers comments.controller');

...

router
    .route('/:bookId/comments')
    .get(controller_comments.getAll) 
    .post(controller_comments.addOne); 

router
    .route('/:bookId/comments/:commentId')
    .get(controller_comments.getOne) 
    .put(controller_comments.updateOne)
    .delete(controller_comments.deleteOne); 
```



### Users

Dans le dossier `routes`, nous allons modifier le fichier `users.js` dans un premier temps nous allons le renommer en `users.route.js` puis nous allons le remplir par les lignes de code suivante :

```
var express = require('express');
var router = express.Router();

var mongoose = require('mongoose');
var config = require('../config/database');
require('../config/passport')(passport);
var jwt = require('jsonwebtoken');
var User = require("../models/users.model");

/* GET users listing. */
router.get('/', function (req, res, next) {
  res.send('respond with a resource');
});

router.
  post('/signup', function (req, res) {
    if (!req.body.username || !req.body.password) {
      res.json({ success: false, msg: 'Please pass username and password.' });
    } else {
      var newUser = new User({
        username: req.body.username,
        password: req.body.password
      });
      // save the user
      newUser.save(function (err) {
        if (err) {
          console.log(err);
          return res.json({ success: false, msg: 'Username already exists.' });
        }
        res.json({ success: true, msg: 'Successful created new user.' });
      });
    }
  });

router
  .post('/login', function (req, res) {
    User.findOne({
      username: req.body.username
    }, function (err, user) {
      if (err) throw err;

      if (!user) {
        res.status(401).send({ success: false, msg: 'Authentication failed. User not found.' });
      } else {
        // check if password matches
        user.comparePassword(req.body.password, function (err, isMatch) {
          if (isMatch && !err) {
            // if user is found and password is right create a token
            var token = jwt.sign(user.toJSON(), config.secret);
            // return the information including token as JSON
            res.json({ success: true, token: 'JWT ' + token });
          } else {
            res.status(401).send({ success: false, msg: 'Authentication failed. Wrong password.' });
          }
        });
      }
    });
  });

module.exports = router;
```

## Les Controllers

A la racine de votre projet, nous allons créer un dossier `controllers`.

### Books

Nous allons créer un fichier `books.controller.js`. Ce fichier comportera toutes nos méthodes en lien avec les books.

```
const Books = require("../models/books.model");

const controller = {
    getAll: (req, res, next) => {
        Books.find({})
            .then(
                books => {
                    res.statutCode = 200;
                    res.setHeader("Content-Type", "application/json");
                    res.json(books);
                },
                err => next(err)
            )
            .catch(err => next(err));
    },
    addOne: (req, res, next) => {
        Books.create(req.body)
            .then(
                book => {
                    res.statutCode = 200;
                    res.setHeader("Content-Type", "application/json");
                    res.json(book);
                },
                err => next(err)
            )
            .catch(err => next(err));
    },
    getOne: (req, res, next) => {
        Books.findById(req.params.bookId)
            .then(
                book => {
                    res.statutCode = 200;
                    res.setHeader("Content-Type", "application/json");
                    res.json(book);
                },
                err => next(err)
            )
            .catch(err => next(err));
    },
    updateOne: (req, res, next) => {
        Books.findByIdAndUpdate(req.params.bookId, {$set: req.body}, {new: true})
            .then(
                book => {
                    res.statutCode = 200;
                    res.setHeader("Content-Type", "application/json");
                    res.json(book);
                },
                err => next(err)
            )
            .catch(err => next(err));
    },
    deleteOne: (req, res, next) => {
        Books.findByIdAndRemove(req.params.bookId)
            .then((resp) => {
                    res.statutCode = 200;
                    res.setHeader("Content-Type", "application/json");
                    res.json(resp);
                },
                err => next(err)
            )
            .catch(err => next(err));
    }
};

module.exports = controller;
```

### Comment

Nous allons à présent créer notre controller pour ajouter un commentaire et une note pour nos livres. Nous allons créer un fichier que nous allons appeler `comments.controller.js` puis nous allons le remplir par les lignes de code suivantes : 

```
const Books = require("../models/books.model");

const controller_comments = {

    getAll: (req, res, next) => {
        Books.findById(req.params.bookId)
            .then(
                books => {
                    res.statusCode = 200;
                    res.setHeader("Content-Type", "application/json");
                    res.json(books.comments);
                },
                err => next(err)
            )
            .catch(err => next(err));
    },

    addOne: (req, res, next) => {
        var token = getToken(req.headers);
        if (token) {
            Books.findByIdAndUpdate(req.params.bookId, { $push: { comments: req.body }})
            .then(
                book => {
                    res.statutCode = 200;
                    res.setHeader("Content-Type", "application/json");
                    res.json(book.comments);
                },
                err => next(err)
            )
            .catch(err => next(err));
        }
        else {
            return res.status(403).send({success: false, msg: 'Not authenticate'});
        }
        
    },


    getOne: (req, res, next) => {
        Books.findById(req.params.bookId)
            .then(
                book => {
                    res.statusCode = 200;
                    res.setHeader("Content-Type", "application/json");
                    res.json(book.comments.id(req.params.commentId));
                },
                err => next(err)
            )
            .catch(err => next(err));
    },

    updateOne: (req, res, next) => {
        var token = getToken(req.headers);
        if (token) {
            Books.update({
                "_id": req.params.bookId,
                "comments._id": req.params.commentId
            }, {
                    $set: {
                        "comments.$.rating": req.body.rating,
                        "comments.$.comment": req.body.comment,
                        "comments.$.author": req.body.author
                    }
                },
                { new: true }
            )
                .then(
                    book => {
                        res.statutCode = 200;
                        res.setHeader("Content-Type", "application/json");
                        res.json(book);
                    },
                    err => next(err)
                )
                .catch(err => next(err));
        }
        else {
            return res.status(403).send({success: false, msg: 'Not authenticate'});
        }
        
    },

    deleteOne: (req, res, next) => {
        var token = getToken(req.headers);
        if (token) {
            Books.findByIdAndUpdate(req.params.bookId, { $pull: { comments: { "_id": req.params.commentId } } })
            .then(
                resp => {
                    res.statusCode = 200;
                    res.setHeader("Content-Type", "application/json");
                    res.json(resp);
                },
                err => next(err))
            .catch(err => next(err));
        }
        else {
            return res.status(403).send({success: false, msg: 'Not authenticate'});
        }
    }
}
```

## Les Models

Nous allons créer les différents modèles de notre application.
Commençons par créer un dossier `models`.
Ces modèles feront le lien entre notre base de données et notre API.

### Books 

Dans ce dossier créer un fichier `books.models.js`. 

```
const mongoose = require("mongoose");
const Schema = mongoose.Schema;
require('mongoose-currency').loadType(mongoose);
const Currency = mongoose.Types.Currency;
   
const bookSchema = new Schema({
    title: {type: String, required: true},
    author: {type: String, required: true},
    image: {type: String},
    year: {type: Number},
    price: {type: Currency},
    description: {type: String, required: true},
    bestseller: {type: Boolean, default: false},
    comments: [commentSchema]
}, {timestamps: true});
   
const Books = mongoose.model('Book', bookSchema);
module.exports = Books;
```

Nous venons d'utiliser le type de monnaie de mongoose, il faut donc l'installer avant. Rendez-vous dans votre terminal et taper la commande suivante : 

```
$ npm install mongoose-currency --save
```


Nous allons ajoutez le modèle relié aux livres qui va nous permettre d'ajouter un commentaire pour cela rajoutez avant la création de l'user les lignes de code suivantes : 

```
...
const commentSchema = new Schema({
    rating: {type: Number, min: 1, max: 5, required: true},
    comment: {type: String, required: true},
    author: {type: mongoose.Schema.Types.ObjectId, ref: 'User'}
}, {timestamps: true});
```

### User

Dans le dossier `models` créer un fichier `users.models.js`. 
Remplissez ce fichier par les lignes de code suivantes :

```
const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const passport = require("passport-local");
var bcrypt = require('bcrypt-nodejs');

var userSchema = new Schema(
    {
        username    : { type: String, unique: true, required: true },
        password    : { type: String, required: true },
        first_name  : { type: String },
        last_name   : { type: String },
        admin       : { type: Boolean}
    },
    { timestamps: true }
);

userSchema.pre('save', function (next) {
    var user = this;
    if (this.isModified('password') || this.isNew) {
        bcrypt.genSalt(10, function (err, salt) {
            if (err) {
                return next(err);
            }
            bcrypt.hash(user.password, salt, null, function (err, hash) {
                if (err) {
                    return next(err);
                }
                user.password = hash;
                next();
            });
        });
    } else {
        return next();
    }
});

userSchema.methods.comparePassword = function (passw, cb) {
    bcrypt.compare(passw, this.password, function (err, isMatch) {
        if (err) {
            return cb(err);
        }
        cb(null, isMatch);
    });
};

const Users = mongoose.model("User", userSchema);

module.exports = Users;
```

### Lancement de l'application 

Nous allons à présent tester notre application avec la commande suivante : 

```
$ npm start
```

Nous pouvons voir notre application à l'adresse [localhost:3000/books](localhost:3000/) sur votre navigateur internet.


## Passport-local

Nous allons installer `passport-local`, pour cela tapez la commande suivante : 

```
$ npm install passport-local --save
```

Nous allons créer un dossier de configuration nommé `config` dedans nous allons créer un fichier nommé `database` et nous allons mettre les informations pour faire le lien entre notre base de données et notre application : 

```
module.exports = {
    'secret':'nodeauthsecret',
    'database': 'mongodb://localhost:27017/amazon'
  };
``` 

Dans le fichier `app.js` nous allons remplacer quelque ligne de code pour faire fonctionner avec notre nouveau fichier database.

Remplacer :  

```
var database = 'mongodb://localhost:27017/amazon';

const connect = mongoose.connect(database, {useNewUrlParser: true });
connect.then (
  db => {
    console.log("Connected correctly to the server");
  },
  err => {
    console.log(err);
  }
);
```

par 

```
var config = require('./config/database');

const connect = mongoose.connect(config.database, {useNewUrlParser: true });
connect.then (
  db => {
    console.log("Connected correctly to the server");
  },
  err => {
    console.log(err);
  }
);
```

Et nous allons aussi ajouter dans le même fichier les lignes de codes suivantes : 

```
var passport = require("passport");

app.use(passport.initialize());
app.use(passport.session());

```





Nous allons ensuite créer un second fichier que nous allons appeler `passport.js`. 

Et nous allons ajouter les lignes de code suivantes dedans : 

```
var JwtStrategy = require('passport-jwt').Strategy,
    ExtractJwt = require('passport-jwt').ExtractJwt;

// load up the user model
var User = require('../models/users.model');
var config = require('../config/database'); // get db config file

module.exports = function(passport) {
  var opts = {};
  opts.jwtFromRequest = ExtractJwt.fromAuthHeaderWithScheme("jwt");
  opts.secretOrKey = config.secret;
  passport.use(new JwtStrategy(opts, function(jwt_payload, done) {
    User.findOne({id: jwt_payload.id}, function(err, user) {
          if (err) {
              return done(err, false);
          }
          if (user) {
              done(null, user);
          } else {
              done(null, false);
          }
      });
  }));
};

```

## CORS

Le CORS permet de prendre en charge des requêtes multi-origines sécurisées et des transferts de données entre des navigateurs et des serveurs web.

Pour rendre notre application CORS, nous devons installer `CORS` pour cela utiliser la commande suivante : 

```
$ npm install cors --save
```

Une fois `cors` installer rendez-vous dans le fichier `app.js` et ajoutez les lignes de code suivante : 

```
...
var cors = require('cors');

var app = express(); // Cette ligne est dèjà présente dans le fichier, il faut ajouter la ligne suivante après la création de la variable app.

app.use(cors());
```

## HTTPS

Pour rendre notre application HTPS nous allons devoir générer deux fichiers nous aurons besoin d'`openSSL` pour cela rendez-vous sur l'adresse suivante pour télécharger : [openSSL](https://www.openssl.org).

Une fois téléchargé rendez-vous dans votre dossier puis taper la commande suivante : 

```
$ openssl req -x509 -newkey rsa:2048 -keyout key.pem -out cert.pem -days 365
```

Il ne vous reste plus qu’à répondre aux questions et à la fin 2 fichiers seront créés : `cert.pem` et `key.pem`.

Ensuite rendez-vous dans le fichier `app.js` et entrez les lignes de code suivantes : 

```
...
var fs = require('fs');
var https = require('https');

var app = express(); // Cette ligne est dèjà présente dans le fichier, il faut ajouter la ligne suivante après la création de la variable app.

https.createServer({
  key: fs.readFileSync('key.pem'),
  cert: fs.readFileSync('cert.pem')
}, app).listen(443);
```

Il se peut que vous rencontriez une erreur dans ce cas, il faut supprimer le mot de passe avec la commande suivante : 

```
$ openssl rsa -in key.pem -out newkey.pem
```

Renommez le fichier `newkey.pem` en `key.pem`.

Nous allons maintenant rediriger notre application vers https.
Pour ce faire nous allons utiliser un module spécifique, installer via la commande suivante :

```
$ npm install express-force-ssl --save
```

Rendez-vous dans le fichier `app.js` et ajoutez les lignes suivantes : 

```
...

var forceSsl = require('express-force-ssl');

...

app.use(forceSsl);

```


Vous pourrez alors vous connecter sur votre serveur https en allant sur l’url suivante : [https://localhost:443](https://localhost:443).

## PostMan 

![PostMan](https://cdn-images-1.medium.com/max/1600/1*QOx_tPV5wJnhTzAGhfIiLA.png)

### Testons notre API avec Postman

Postman est un logiciel permettant de tester facilement les requêtes HTTP, il se focalise sur les tests d'API. Il permet d'envoyer toutes sorte de requêtes et de les personnaliser mais aussi de gérer les authentifications, les scripts de test, etc.

Vous pouvez télécharger le logiciel à cette adresse : [Télécharger Postman](https://www.getpostman.com).

![Postman](https://image.noelshack.com/fichiers/2019/06/1/1549314971-postman.png)

Voici les principales fonctionnalités à connaitre pour les tests :

* (1) : Choix du type de requête (GET, POST, PUT, DELETE, etc.)
* (2) : Url de l'API 
* (3) : Les paramètres à passer dans l'Url
* (4) : Tout ce qui constitue une requête HTTP (Header, Body, etc.)

### Test un POST 

Pour faire un test sur un POST, sélectionnez POST puis ajoutez l'url, cliquez sur l'onglet Body puis sélectionnez Raw pour définir manuellement le contenu de la requête HTTP, enfin dans l'onglet le plus à droite sélectionnez JSON (application/json). Et pour finalisez le test entrez un JSON avec les informations et appuyez sur le bouton `Send`.

Rien ne se passe ? C'est Ok, mais nous pouvons constatez une réponse avec le code 201. 

Pour vérifier que notre POST a bien marché, il suffit d'effectuer un GET pour constater l'ajout.

### Test un GET 

Pour faire un test sur un GET, il suffit simplement de choisir le type de requête, sélectionnez GET puis de rentrer l'url et de valider avec le bouton `Send`.

### Vérifié nos requêtes avec un Token 

Pour vérifier que notre application dispose bien d'un système de sécurité lors d'une connexion, nous allons tester le `passport-local` ajoutez précédemment. 

Nous allons créer un utilisateur avec un POST et l'url suivant : `http://localhost:3000/users/signup`. Puis nous remplissons le `Body` avec les bonnes informations pour la création d'un utilisateur à savoir un username et un password obligatoire, une fois créer nous lançons le test avec le bouton `Send` puis dans l'onglet en bas de postman nous récupérons un token. 
Nous allons copier se token puis nous allons effectuer une vérification grâce à une authentification.

Pour nous authentifier nous devons utiliser un GET avec l'url suivante : `http://localhost:3000/users/signin`, dans l'onglet `Headers` nous allons ajoutez un moyen d'authentification pour cela coché la case `Authorization` puis dans la case `Value` nous allons coller le Token copié précédemment.

Félicitation votre application possède un moyen sur de création de compte ainsi que de connexion.

### Vérifié nos requêtes avec HTTPS
 
Pour vérifier que notre serveur répond bien au protocole HTTPS nous allons dans un premier temps ajouter le certificat créer précédemment pour notre serveur HTTPS.

Pour cela rendez-vous dans les paramètres de PostMan dans la section `Général` puis désactiver `SSL Certificate verification`.

Ensuite nous allons rajouter notre certificat aller dans la section `Certificates` puis ajoutez l'url suivante dans la partie host `https://localhost:443` ensuite rajoutez les deux fichiers créer précédemment le fichier `key.pem` ainsi que le fichier `Cert.pem` enfin rajoutez la phrase rentrée lors de la création de nos fichiers puis valider l'ajout de notre certificat.

Pour tester notre application avec le `HTTPS` nous devons absolument avoir l'URL suivant : `https://localhost:443/`

Pour tester notre application avec le `HTTP` nous devons absolument avoir l'URL suivant : `http://localhost:3000/`

## Bonus 

### Quelques commandes git 

Nous allons voir les commandes principales à utiliser pour mener à bien ce projet.

Tout d'abord rendez vous dans votre répertoire avec la commande suivante : 

```
$ cd ~/mon_repertoire_de_projet
```

Pour relier notre répertoire avec un repositorie Git, tapez les commandes suivantes : 


```
$ git init 
$ git remote add origin <lien de votre repositorie>
```

Chaque fois que vous créez/modifiez un fichier sur votre machine vous devrez l'ajoutez à votre repositorie.

Pour cela voici les commandes principales.

* **Permet de voir les nouveaux fichier/modification pas encore envoyez sur le repositorie.**

```
$ git status
```

* **Permet d'ajouter tous les nouveaux fichier/modification.**

```
$ git add .
```

* **Permet d'envoyer sur le serveur local vos modifications.**

```
$ git commit -m "message explicatif du commit"
```

* **Permet d'envoyer sur le serveur distant vos modifications.**

```
$ git push 
```

* **Permet de récupérer les fichiers sur le serveur distant.**

```
$ git pull
```

Lors de votre 1er envoi sur le serveur distant, vous devrez communiquez vos identifiant ainsi que votre mot de passe.


### .gitignore 

Nous allons à présent créer un fichier .gitignore qui va permettre d'ignorer certains fichiers.

Cela permet par exemple de ne pas envoyer sur le repositorie en ligne certain fichier contenant des mots de passe ou configurations, ou encore des fichiers générés automatiquement par certain IDE ou machine.

Dans **Visual Studio Code**, créer un nouveau fichier que vous nommerez `.gitignore` et vous allez ajouter les lignes de code du lien suivant : [gitignore](https://github.com/github/gitignore/blob/master/Node.gitignore).

Maintenant tous vos prochains commit ne contiendront plus les fichiers ajoutés à votre gitignore.

Si vous avez un fichier sur votre repositorie que vous voulez retirer car vous avez décidez de le mettre dans votre `.gitignore`, tapez les commandes suivantes : 

```
$ git rm --cached <le nom du fichier à retirer>
$ git commit -m "retrait du fichier en question"
$ git push
```

Voilà votre fichier ne fait plus partie du repositorie.


***Par Halfon Nessim***
